This section will introduce the **guide** and the **virtual lab environment**. 

- The initial preparation for the lab involves **downloading and installing** the appropriate **software and configuration files** needed to complete the guide (Chapter 2).

- Following this you will **set up the System Management Server (SMS)** host (Chapter 3). The **smshost** is responsible for managing the virtual cluster. 

- Next you will **prepare the compute node image** (Chapter 4) such that the compute node virtual machines can be provisioned by *Warewulf* (Chapter 5).

- The remainder of the guide involves further **preparing the virtual cluster to run HPC applications** and **running test jobs** in the virtual cluster environment using *Slurm*. 
 

!!! quote "Note"

    Anticipated time taken to complete this chapter: ?? minutes

!!! danger "Important"

    This guide uses *VirtualBox* for the **virtual lab environment**.

    *Vagrant* is used to manage the VirtualBox **Virtual Machine (VM) configuration**.

    Although it is technically feasible to run a VM as part of a physically deployed HPC system, YOU DO **NOT** NEED VIRTUALBOX OR VAGRANT FOR THE FINAL HPC DEPLOYMENT.

    *VirtualBox* and *Vagrant* are used within this virtual lab to **simulate a virtual cluster** and **simplify the VM deployment process**, respectively.



## 1.1 VM Setup using Vagrant
---
Within this virtual lab, *Vagrant* is used to define and manage virtual machines in the *VirtualBox* environment. This approach ensures a straightforward and consistent VM deployment for all users. There exists a *Vagrant* definition for both the **smshost** and the **compute hosts**. These host definitions are captured in a file called a `Vagrantfile`.

### 1.1.1 **Smshost**

The **smshost** VM is instantiated with a base Rocky Linux 8 operating system image. This is the standard software environment that is used in the official OpenHPC install recipe. For this virtual lab the image is sourced from the *Bento* provider through *Vagrant Cloud*.

The **smshost** is defined as follows:

- **Rocky Linux 8 Image**
- **1GB RAM**
- **2 virtual CPUs**
- **Hostname: *smshost***
- **External virtual network interface**: for *internet connectivity* and *ssh* access
- **Internal virtual network interface**: for communication over the internal cluster network (**hpcnet**)


The custom **software additions** to the base VM image are: 

- Extra **packages**: 
    - Such as `tmux`, `vim` and `Git` 
- The environment **configuration file** `input.local.lab` 


### 1.1.2 Compute hosts

The `Vagrantfile` definition for the **compute hosts** differs to that of the **smshost**. This is because the image for these hosts is provided by *Warewulf* via **PXE** on boot. Because the compute nodes are **stateless**, their images are configured and stored on the **smshost** and loaded into memory on boot. This will be explained further and demonstrated in the chapters that follow. 

For these hosts, *Vagrant* requires an 'empty' box file (no image) that can be used as a base and upon which the hardware configuration for the host can be built using the `Vagrantfile`. This file, `package.box`, is stored locally in the lab directory pulled from the GitLab repo.

The **compute hosts** have the following *Vagrant* definition:

- **No image**
- **3GB RAM**
- **2 virtual CPUs**
- **Hostname: *compute[00-01]***
- **Internal virtual network interface**: for communication over the internal cluster network (**hpcnet**)


## 1.2 OpenHPC
---

The *OpenHPC* guide relies on `${variable}` substitution in the command line. Although it is possible to manually substitute these variables at each step, it is **recommended** to use the **`input.local.lab`** file to define IP adresses, hardware MAC adresses, and so on. 

!!! note "Tip"

    The default values in `input.local.lab` have been thoroughly tested for the virtual lab deployment as outlined in the guide. It is strongly recommended that you do not modify them.

## 1.3 Cluster Layout
---

This virtual lab involves configuring three lightweight virtual machines that are used to create a virtual cluster using the *VirtualBox* hypervisor. The layout of the virtual cluster that will be deployed is depicted in Figure 1. 

!!! danger "Important"

    Make sure that you understand the overview, because you will be adding your specifications to the figure.

!!! quote "Note"

    Whereas the original OpenHPC recipe expects four compute nodes, we will work with two. 

[<img id="fig1" alt="Figure 1: Overview of the Virtual Cluster layout" src="../_media/figure1.png" />]()
<center>Figure 1: Overview of the Virtual Cluster layout</center>
