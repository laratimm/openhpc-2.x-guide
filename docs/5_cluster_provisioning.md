This section covers the provisioning of the HPC cluster using *Warewulf*. By the end of this section you will have a working, networked set of compute and control machines that form the foundation of the HPC system.

## 5.1 Finalise Configuration

In the preceding chapters we completed the following steps:

- configured the virtual cluster environment, and 
- prepared the **smshost** software stack, and 
- defined the compute node images for administration through *Warewulf*. 
 
Next, we need to provision the compute nodes. In order to do so, we create a bootstrap image which is used to boot the nodes and complete provisioning. 

Create the boostrap image with the following command:

```bash
[root@smshost ~]# sudo wwbootstrap `uname -r` 

# OUTPUT:
#
# Number of drivers included in bootstrap: 513
# Building and compressing bootstrap
# Integrating the Warewulf bootstrap: 4.18.0-425.3.1.el8.x86_64
# Including capability: provision-adhoc
# Including capability: provision-files
# Including capability: provision-selinux
# Including capability: provision-vnfs
# Including capability: setup-filesystems
# Including capability: setup-ipmi
# Including capability: transport-http
# Compressing the initramfs
# Locating the kernel object
# Bootstrap image '4.18.0-425.3.1.el8.x86_64' is ready
# Done.
```

While most of the provisioned image's configuration is conducted in a `chroot` filesystem, these `chroots` cannot be directly provisioned by *Warewulf*. 

Once we are satisfied with our `chroot` configuration, we must encapsulate and compress this filesystem into a *Virtual Node File System (VNFS)* image which *Warewulf* can provision. 

You can think of the `chroot` behaving as the source code, and the *VNFS* behaving as the compiled binary of that source. 

```bash
[root@smshost ~]$ sudo wwvnfs --chroot $CHROOT 

# OUTPUT
# _FORTIFY_SOURCE requires compiling with optimization (-O) at /usr/lib64/perl5/features.ph line 207.
# Using 'rocky8.5' as the VNFS name
# Creating VNFS image from rocky8.5
# Compiling hybridization link tree                           : 0.41 s
# Building file list                                          : 1.36 s
# Compiling and compressing VNFS                              : 160.99 s
# Adding image to datastore                                   : 27.36 s
# Wrote a new configuration file at: /etc/warewulf/vnfs/rocky8.5.conf
# Total elapsed time                                          : 190.11 s
```

Lastly, we set the network interface to use for provisioning of the nodes (`$eth_provision`). By default, *Warewulf* uses legacy network names for Linux machines (`eth0`, `eth1` .. `ethN`). This can be changed to predictable names, but requires additional configuration and is out-of-scope for this guide.

```bash
[root@smshost ~]# sudo echo "GATEWAYDEV=${eth_provision}" > /tmp/network.$$ 
[root@smshost ~]# sudo wwsh -y file import /tmp/network.$$ --name network 
[root@smshost ~]# sudo wwsh -y file set network --path /etc/sysconfig/network --mode=0644 --uid=0      

# OUTPUT
# About to apply 3 action(s) to 1 file(s):
# 
#     SET: PATH                 = /etc/sysconfig/network
#     SET: MODE                 = 0644
#     SET: UID                  = 0
# 
# Proceed?
```

## 5.2 Register Nodes

Now that the base configuration is done for the *Warewulf* server and the compute node configurations are staged within *Warewulf*, we must add the node definitions to the *Warewulf* data store.

!!! Tip
    You can run `wwsh node list` to check the list of node definitions before running the loop, and then again after running the loop, to see how nodes are added and defined in the *Warewulf* data store.

```bash
[root@smshost ~]# sudo for ((i=0; i<$num_computes; i++)) ; do  
    wwsh -y node new ${c_name[i]} --ipaddr=${c_ip[i]} --hwaddr=${c_mac[i]} -D ${eth_provision}
done 
```

The node entries have now been defined through the `wwsh -y node new` command (`wwsh node list` confirms this) and now we will set the provisioning image that is to be used for the compute nodes. We can also use the following step to specify which imported files we want to use in the provisioning process.

```bash
[root@smshost ~]# sudo wwsh -y provision set "${compute_regex}" --vnfs=rocky8.5 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,munge.key,network 
```

After the above changes, we must restart the DHCP server to update the `dhcpd.conf` and update the *Warewulf* PXE database.

```bash
[root@smshost ~]# sudo systemctl restart dhcpd 
[root@smshost ~]# sudo wwsh pxe update 
```

!!! note "Note"
    *Warewulf* does not replace the standard Linux services (such as DHCP) but acts as a frontend to manage these services. For troubleshooting and investigation, the standard files involved with PXE, DHCP, NFS, etc. can still be investigated as usual.

## 5.3 Boot Compute Nodes

Now is the time for us to start up the other virtual machines that form part of the *Vagrant* specification file. Exit out of **smshost**, or use another terminal window, to launch the other virtual machines with the following commands:

```bash
[~/openhpc2.x/]$ vagrant up compute00 

[~/openhpc2.x/]$ vagrant up compute01 
```

!!! Warning
    Please take note that you will see the following error message when running the above commands:
    > The requested communicator '' could not be found.
    > Please verify the name is correct and try again.
    
    This can be safely ignored. It has no impact on the lab.

!!! success Congratulations 
    Your compute nodes are now successfully booted up and ready!

!!! example Recap: Chapter 5