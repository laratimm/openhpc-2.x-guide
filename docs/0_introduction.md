
This document presents a step by step guide towards the deployment of a **virtual HPC cluster** using the open-source, community-driven software suite - [OpenHPC](https://openhpc.community/){:target="_blank"}. It is designed to serve as a <ins>supporting guide</ins> to the [official OpenHPC 2.x Install Recipe](https://openhpc.community/downloads/). 

Although the official OpenHPC&nbsp;2.x install recipe is perfectly suitable for installing the OpenHPC stack on a multitude of system environments, this OpenHPC-2.x guide steers towards a <ins>specific implementation</ins> of the OpenHPC software stack on a *virtual cluster*, within a virtual lab environment. 

!!! quote "Keywords"

    - OpenHPC
    - HPC Ecosystems
    - Introduction
    - HPC
    - Virtual Lab
    - Hands-on
    - Virtual Cluster
    - System Administrator

### Background

This guide was produced by the **Advanced Computer Engineering (ACE) Lab** at the **Centre for High Performance Computing (CHPC)**, for the [HPC Ecosystems Project community](https://ecosystems.nicis.ac.za/). It functions as a standalone self-guided walkthrough for depolying **OpenHPC&nbsp;2.x (Warewulf/Slurm/RockyLinux8)** in a virtual lab on virtual machines (rather than on physical hardware).  

### Acknowledgements

This guide owes its existence to a team of **contributors**:

- Lara Timm
- Bryan Johnston
- Eugene de Beste
- Mmabatho Hashatsi

## 0.1 Objectives and Outcomes 
--------

The purpose of this document is to guide the reader to **deploy a basic operational virtual cluster using OpenHPC**. The material is presented as a step-by-step walkthrough of the software stack deployment. While the guide does not stictly focus on teaching, it is hoped that some understanding and knowledge will be gained through completing the hands-on virtual deployment. 

This virtual lab intends to act as a hands-on learning environment to serve as a *precursor* for the deployment of a **physical High Performance Computing (HPC) system**, using the same approach as outlined in this guide. 

## 0.2 Target Audience 
--------

The guide is targeted at site-designated **HPC System Administrators** of the HPC Ecosystems community. The content, however, is **universal** and will be relevant to anyone who wishes to practise deploying an *OpenHPC 2.x cluster in a virtual lab environment*. 

## 0.3 Requirements 
--------

To successfully complete this guide, the following is **required**:

- Computing resources capable of hosting the Virtual Lab (see Prerequisites).
- Basic Linux shell experience
- Basic understanding of HPC and parallel computing

## 0.4 Assumptions 
--------

The official OpenHPC install recipe, which this guide is designed to compliment, is targeted at **experienced Linux system administrators** for HPC environments. 

This guide seeks to compliment the OpenHPC install recipe by **bridging the gap** for those that are new to HPC environments, and have basic (but still **some**) Linux system administrator experience. If you are able to answer *YES* to the following questions, then you should experience no difficulty in completing this guide. 

!!! quote ""

    - Do you know how to navigate the file system using the Linux shell?
    - Have you installed packages via the Linux shell using yum, apt or compilers?
    - Do you know how to stop and start services in a Linux shell?
    - Do you understand basic principles of computer networking such as IPv4, PXE, DNS?
    - Do you know what High Performance Computing is and why you are learning to install a system that supports it?

If you answered *NO* to any of the previous questions, it may be worth revisiting the source of the question and ensuring you are comfortable with the details before going any further. 

!!! note "Tip"

    This guide is not going away, so take your time and be sure you are comfortable to continue!


## 0.5 Prerequisites 
### 0.5.1 Resources
--------
#### Virtual Lab Deployment

When following this guide you will deploy an **OpenHPC-ready** **VirtualBox Virtual Machine (VM)** using *Vagrant*. This VM will act as your **management node**, and will manage your virtual cluster. Although we do not detail the use of hypervisors other than VirtualBox, an appropriately configired *Vagrant* definition should allow you to achieve the same results on another hypervisor of your choice (e.g. *VMware*).

#### Virtual Cluster Deployment

To demonstrate the configuration and use of a virtual cluster, two additional low-performance VM's (**compute nodes**) will be spawned.

!!! quote "Note"

    In a standard physical HPC environment, your compute nodes are typically **not** low-performance. This is done in this lab to constrain the resource usage on the workstation you are using to complete the virtual lab. 

In order to support this *three-node virtual cluster*, we recommend that your host machine (the one you are completing this lab on) should have at least:

- **10GB available storage** (20GB+ is preferred)
- **4GB available RAM (8GB+ is preferred)**
    - The *smshost VM* must have a minimum of **1GB RAM** (1024MB).
    - the *compute host VMs* must have a minimum of **3GB RAM** each (3072MB). 

!!! note "Tip"

    This guide has tested with *VirtualBox* and *Vagrant*, as per the steps outlined in the sections that follow. If you follow the same software stack implementation, you should expect to enjoy similar results.


The following files and packages will be required to get you started:


| Resource                                 | Link                                                                   |
|------------------------------------------|------------------------------------------------------------------------|
| Oracle VirtualBox (~100MB)               | <https://www.virtualbox.org/>{:target="_blank"}                        |
| Hashicorp Vagrant (~250MB)               | <https://www.vagrantup.com/>{:target="_blank"}                         |
| Ecosystems OpenHPC2.x GitLab folder      | Detailed in Section 2.3                                              |




### 0.5.2 Workload and Time
--------

Once you have downloaded the intial resources, installing the various components should take approximately 10-15 minutes. This process involves **VirtualBox installation**, **Vagrant installation** and **cloning the required lab files** from the [Ecosystems GitLab](https://gitlab.com/hpc-ecosystems/training/openhpc-2.x/). This, however, is only the beginning. 

The *actual time* it will take to complete this guide and **deploy a successful virtual cluster** will depend on a number of factors. These may include:
 
- The speed of your internet connection
- Your level of familiarity with the Linux command line and the syntax/commands used in the guide
- How much time you invest into understanding the steps that you complete along the way
- Your willingness to read the guide thoroughly before executing each step **(HIGHLY RECOMMENDED)**
- Your familiarity with the HPC design being implemented in the guide
- Your willingness to plan before executing


!!! note "Tip"

    **Read the instructions carefully!**

    Make sure that you understand the instructions before executing them. You must know what it is that you are doing so that you can fix things if something doesnt work as expected. 

!!! quote "Note"

    When following the **official OpenHPC install recipe**, one is required to configure the file `input.local` according to your system.

    Since this is a **virtual lab** (and to make things easier for the user) `input.local` is **preconfigured and replaced** with a simplified version - `input.local.lab`. 

    Please ensure that you **read through and understand `input.local.lab`** so that you can identify where things may have gone wrong, if they do, later in the guide!


## 0.6 Why this Guide? 
--------

The main purpose of this training material is to provide a robust, easy-to-follow, standalone guide to be used by participating HPC Ecosystems partner sites as a virtual training activity for the deployment of their HPC Ecosystems hardware. 

A large amount of time, research and peer review was undertaken to develop what we believe to be the best possible Virtual OpenHPC 2.x training material for the HPC Ecosystems Community. At the time of this content development, the only other public on-demand OpenHPC virtual training material was our own [OpenHPC 1.3.x virtual guide](https://hpc-ecosystems.gitlab.io/training/openhpc-101/#/){:target="_blank"} (to which this guide is the sequel!). 

## 0.7 Materials Used 
--------

This OpenHPC 2.x virtual lab integrates the following materials:

- The official OpenHPC 2.x (Warewulf/Slurm) install recipe
- The HPC Ecosystems OpenHPC 1.3.x virtual training guide


## 0.8 Conventions 
--------

The examples in this guide follow a number of conventions:

1. **Input boxes** are displayed as code boxes, where:
    - The text to the left of the **#** symbol is the current working directory or path
    - The text to the right of the **#** symbol is the input parameters
    
    For example:

    ``` bash
    [~/openhpc-2.x-virtual-lab/]# vagrant ssh
    ```

2. **Variable substitution** is indicated in two ways:
    - Within **arrow brackets**. ie. `<your_variable_here>`
    - Using **environment variables** defined in `input.local.lab`. ie. `${environment_variable}`


## 0.9 Key 
--------

Additional information, tips, things to note, milestones and recaps are captured is this guide using the following callout box styles: 

!!! quote "Note"

    Notes are intended as **additional background information or observations** to address obvious queries / questions / anomalies.

    They will be presented in grey boxes.

!!! note "Tip"

    Tips are intended to **provide additional functionality and user tips** to improve the user experience and deployment process.

    They will be presented in blue boxes.

!!! danger "Important"

    Important warnings are included to advise of **common pitfalls or mis-steps** that may have a significant (or irreversible) impact on the deployment process.

    They will be presented in red boxes.

!!! example "Recap"

    Recaps are intended to provide a **summary of the important concepts and topics** that have been covered at the conclusion of the current section.

    They will be presented in purple boxes.

!!! success "Congratulations"

    Milestones will be presented to **mark significant achievements** in the deployment process; these are also useful for conveying progress markers when seeking feedback or assistance or progress reports.

    They will be presented in green boxes

