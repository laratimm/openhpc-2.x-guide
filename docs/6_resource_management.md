# 6. Resource Management

## 6.0. Introduction to Chapter
In this chapter we will configure and start up the *Slurm Workload Manager* ("*Slurm*"). This must be performed on both the System Management Server host (**smshost**) and all compute nodes. *Slurm* uses the *MUNGE* authentication service to manage credentials. As such, the *MUNGE* daemon must be running on both the master host and the compute nodes within the resource management pool.

## 6.1. Slurm Configuration

1. To start the *Slurm* configuration we will make use of the ohpc-provided file. <div>

```bash
[root@smshost ~]# sudo cp /etc/slurm/slurm.conf.ohpc /etc/slurm/slurm.conf
[root@smshost ~]# sudo cp /etc/slurm/cgroup.conf.example /etc/slurm/cgroup.conf
```
2. The default configuration for *Slurm* will not be correct for this lab. View the contents of the default `slurm.conf` file.

```bash
[root@sms-host ~]# cat /etc/slurm/slurm.conf | grep SlurmctldHost

# OUTPUT
# SlurmctldHost=linux0 
```
3. Once content of the default `slurm.conf` file is known, you can modify it to match the environment for the lab. We will replace the default name for the `SlurmctldHost` with the `$sms_name`.

```bash 
[root@smshost ~]# sudo perl -pi -e "s/SlurmctldHost=\S+/SlurmctldHost=${sms_name}/" /etc/slurm/slurm.conf

[root@smshost ~]# cat /etc/slurm/slurm.conf | grep SlurmctldHost

# OUTPUT
# SlurmctldHost=smshost
```
The above *perl* command replaces the default *Slurm* controller name (**linux0**) with the system defined name at `${sms_name}` (the lab default is **smshost**).

    !!! note "Tip"
     A quick test of the very first change we made to `/etc/hosts` will verify that the smshost can identify its own DNS entry.
    
     ```bash
     [root@sms-host ~]$ ping smshost
     ```
     This should return output that looks like the below:
     ```text
     64 bytes from smshost (127.0.0.1): icmp_seq=1 ttl=64 time=0.040 ms
     64 bytes from smshost (127.0.0.1): icmp_seq=2 ttl=64 time=0.164 ms
     `

4. The default *Slurm* configuration file included with *OpenHPC* assumes dual-socket, 8 cores per socket, and 2 threads per core. However, our Virtual Cluster is designed with **1 socket**, **2 cores per socket**, and **1 thread per core**. See Table below:
    |                   | OpenHPC | **HPC Ecosystems** |
    | ----------------- | :-----: | :------------: |
    | Socket Count      | 2       | 1              |
    | Cores per Socket  | 8       | 2              |
    | Threads per Core  | 2       | 1              |

```bash
[root@smshost ~]# cat /etc/slurm/slurm.conf | grep Sockets

# OUTPUT
# NodeName=c[1-4] Sockets=2 CoresPerSocket=8 ThreadsPerCore=2 State=UNKNOWN 
```
!!! note "Tip"
    For this workshop there are various approaches to changing the `slurm.conf` file:
        - Perl regular expressions _(demonstrated)_
        - *Slurm* online configuration tool
        - manual edits
        - replacing `slurm.conf` with `slurm.conf.lab` included in the Git repository

5. Using *Perl* to change the parameters:

```bash
[root@smshost ~]# sudo perl -pi -e "s/NodeName=c\[1-4\]/NodeName=compute0\[0-1\]/" /etc/slurm/slurm.conf
[root@smshost ~]# sudo perl -pi -e "s/Sockets=2/Sockets=1/" /etc/slurm/slurm.conf
[root@smshost ~]# sudo perl -pi -e "s/CoresPerSocket=8/CoresPerSocket=2/" /etc/slurm/slurm.conf
[root@smshost ~]# sudo perl -pi -e "s/ThreadsPerCore=2/ThreadsPerCore=1/" /etc/slurm/slurm.conf
```

Checking the contents of `slurm.conf` should now reveal that the compute nodes are defined as:
- **compute00** and **compute01** each with:
    - 1 Socket
    - 2 CoresPerSocket and 
    - 1 ThreadPerCore

```bash
[root@smshost ~]# cat /etc/slurm/slurm.conf | grep Sockets

# OUTPUT
# NodeName=compute0[0-1] Sockets=1 CoresPerSocket=2 ThreadsPerCore=1 State=UNKNOWN 
```

6. Replace `Nodes=c[1-4]` with `Nodes=compute0[0-1]`. This specifies that the compute nodes defined earlier are added to the default partition, `normal` . The changes can be seen in the `slurm.conf` file.

```bash
[root@smshost ~]# cat /etc/slurm/slurm.conf | grep Nodes

# OUTPUT
# PartitionName=normal Nodes=c[1-4] Default=YES MaxTime=24:00:00 State=UP Oversubscribe=EXCLUSIVE 

[root@smshost ~]# sudo perl -pi -e "s/Nodes=c\[1-4\]/Nodes=compute0\[0-1\]/" /etc/slurm/slurm.conf
[root@smshost ~]# cat /etc/slurm/slurm.conf | grep Nodes

# OUTPUT
# PartitionName=normal Nodes=compute0[0-1] Default=YES MaxTime=24:00:00 State=UP Oversubscribe=EXCLUSIVE 
```

!!! note "Tip"
    The perl command for replacing the `NodeName` uses special break characters (`\`) to accommodate for the character symbols `[` and `]`.

7. In the `slurm.conf` file there are duplicate definitions for `JobCompType` and `TaskPlugin`. In order to fix this and prevent a duplicate definition error, run the following commands:

```bash
[root@smshost ~]# cat /etc/slurm/slurm.conf | grep 'JobCompType\|TaskPlugin' 
 
# OUTPUT
# TaskPlugin=task/affinity
# JobCompType=jobcomp/none
# TaskPlugin=task/affinity
# JobCompType=jobcomp/filetxt

[root@smshost ~]# sudo sed -i '0,/JobCompType\=jobcomp\/none/{/JobCompType\=jobcomp\/none/d;}' /etc/slurm/slurm.conf
[root@smshost ~]# sudo sed -i '0,/TaskPlugin/{/TaskPlugin/d;}' /etc/slurm/slurm.conf
[root@smshost ~]# cat /etc/slurm/slurm.conf | grep 'JobCompType\|TaskPlugin' 

# OUTPUT
# TaskPlugin=task/affinity
# JobCompType=jobcomp/filetxt 
```

## 6.2 Start/Enable Services 

Start/Enable *MUNGE* and *Slurm* services on the master host:

```bash
[root@smshost ~]# sudo systemctl enable munge

# OUTPUT
# Created symlink /etc/systemd/system/multi-user.target.wants/munge.service → /usr/lib/systemd/system/munge.service. 

[root@smshost ~]# sudo systemctl enable slurmctld 

# OUTPUT
# Created symlink /etc/systemd/system/multi-user.target.wants/slurmctld.service → /usr/lib/systemd/system/slurmctld.service. 

[root@smshost ~]# sudo systemctl start munge 
[root@smshost ~]# sudo systemctl start slurmctld
```
Start *Slurm* and related services on compute hosts using *pdsh* (a parallel shell tool to run a command across multiple nodes in a cluster):

!!! Tip
    When the **smshost** first connects to a compute node, it will add the computer to the list of known hosts on **smshost**. 

    For instance, after running the `pdsh` commands below, this will establish the first SSH connection, using SSH keys, thereby adding `compute00` and `compute01` to the list of known hosts.

    ```bash
    compute00: Warning: Permanently added 'compute00,10.10.10.100' (ECDSA) to the list of known hosts.
    compute01: Warning: Permanently added 'compute01,10.10.10.101' (ECDSA) to the list of known hosts.
    ```


```bash
[root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] "systemctl start chronyd" 
[root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] "systemctl status chronyd" | grep running

# OUTPUT
# compute00:    Active: active (running) since Wed 2023-03-29 17:36:10 UTC; 3h 4min ago
# compute01:    Active: active (running) since Wed 2023-03-29 20:40:45 UTC; 18s ago

[root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] "systemctl start munge"
[root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] "systemctl status munge" | grep running

# OUTPUT
# compute01:    Active: active (running) since Wed 2023-03-29 20:40:45 UTC; 50s ago
# compute00:    Active: active (running) since Wed 2023-03-29 17:36:10 UTC; 3h 5min ago

[root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] "systemctl start slurmd" 
[root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] "systemctl status slurmd" | grep running 

# OUTPUT
# compute00:    Active: active (running) since Thu 2023-03-30 06:56:14 UTC; 53min ago
# compute01:    Active: active (running) since Thu 2023-03-30 07:46:15 UTC; 3min 26s ago

```

!!! Tip "libhwloc.so.15 missing" 
    If you encounter a problem starting `slurmd` service on the compute nodes, you can verify the reason by invoking `systemctl status slurmd`. 
    
    If the error relates to `libhwloc.so.15: cannot open shared object file: No such file or directory` then this can be solved with the following steps (NOTE: less common errors are potentially addressed in the Virtual Lab FAQ section);

    ```bash
    sudo dnf --installroot=$CHROOT list hwloc-libs
    ```

    When working with *Warewulf*, any updates to a diskless image's `chroot` must be encapsulated and compressed into a *Virtual Node File System (VNFS)* image that *Warewulf* can provision. As mentioned before, changes to `chroot` is akin to modifying source code, and *VNFS* is the compiled binary from the source code.

    ```bash
    sudo wwvnfs --chroot $CHROOT
    ```
    Once the image is recompiled and added to the datastore:
    - reboot the compute node VM.
    - ```bash
      [root@compute01 ~]# sudo systemctl status slurmd

      # OUTPUT:
      # ● slurmd.service - Slurm node daemon
      # Loaded: loaded (/usr/lib/systemd/system/slurmd.service; disabled; vendor preset: disabled)
      # Active: active (running) since Sat 2023-08-26 11:16:48 UTC; 1min 10s ago
      # Main PID: 1113 (slurmd)
      # Tasks: 2
      # Memory: 1.4M
      # CGroup: /system.slice/slurmd.service
      #         └─1113 /usr/sbin/slurmd -D -s --conf-server 10.10.10.10
      ```    
Verify the *Slurm* subsystem is working correctly by inspecting `sinfo`:
```bash
[root@smshost ~]# sudo sinfo 
```
If everything is configured correctly, `sinfo` should return something that resembles the following output:
```bash
    [root@smshost vagrant]# sinfo
    PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
    normal*      up 1-00:00:00      2   idle compute[00-01]
```
If the **STATE** is **idle** then the compute nodes are talking correctly with the *Slurm* manager.

## 6.3 Prepare for Test Job
 
 With the resource manager successfully enabled, users are able to submit jobs for processing. We will add a `test` user on the **smshost** for running an example job.

```bash
[root@smshost ~]# sudo useradd -m test 
```
!!! quote "Note"

    In chapter 4 we registered credenital files with *Warewulf* (e.g. `passwd`, `group` and `shadow`) so that these files are propogated during compute node imaging. *Warewulf* installs a utility on the compute nodes to automatically synchronize known files from the provisioning server at five minute intervals but with a new `test` user just recently added these files have become outdated for the time being, until the next file synchronisation.

To update the *Warewulf* database to incorporate the additions, run the following resync process:

```bash
[root@smshost ~]# sudo wwsh file resync passwd shadow group 
```

!!! note "Tip"

    As part of the learning exercise, you can run a separate session (using `tmux`, for instance which is pre-installed on the Virtual Lab image) to watch the users file on the compute node, and observe when the file resync is completed. From a compute node:
    ```bash
    watch 'cat /etc/passwd | grep test'
    ```

    After re-syncing (which notifes *Warewulf* of file modifications made on the **smshost**), it will take approximately 5 minutes for the changes to propagate. However, you can also manually force the results as follows:

    ```bash
    [root@smshost ~]# sudo pdsh -w ${compute_prefix}[00-01] /warewulf/bin/wwgetfiles 
    ```

!!! success "Congratulations"

    **You have reached the end of Chapter 6 - Well done!**<br>

!!! example Recap: Chapter 6
    In this chapter you have successfully configured and started up *Slurm* - the HPC cluster's resource manager, which will manage all jobs submitted to the cluster.
