This section will cover the preparation of the compute node image which will be provisioned using **Warewulf**.

!!! quote "Note"

    Anticipated time to complete this chapter: ???. 

!!! note "Tip"

    Keep in mind your ability to take snapshots of your **smshost** VM. You may wish to take snapshots at various milestones to ensure that you are able to quickly recover from any major mistakes. 

## 4.1 Initialise Warewulf

All of the packages required to use **Warewulf** on the *smshost* should already be installed. The next step is to update a number of configuration files which will allow **Warewulf** to work with Rocky 8 and will support local provisioning using a second private interface (refer to **Figure 1**). 

!!! quote "Note"

    By default, **Warewulf** is configured to provision over the `eth1` interface. If you would prefer to use an alternatively named interface, defined by `${sms_eth_internal}`, the steps below should be run to override this default.

1. Configure Warewulf to use the desired internal interface <div>

    ```bash
    [root@smshost ~]# sudo sed -i "s/device = eth1/device = ${sms_eth_internal}/" /etc/warewulf/provision.conf 
    ```

2. Enable the internal interface for provisioning <div>


    ```bash
    [root@smshost ~]# ip link set dev ${sms_eth_internal} up
    [root@smshost ~]# ip address add ${sms_ip}/${internal_netmask} broadcast + dev ${sms_eth_internal} 
    ```
Whether or not you overwrote the default interface, you are now required to restart and enable the relevant services that are required for provisioning with *Warewulf*. This can be done as follows:

```bash
[root@smshost ~]# sudo systemctl enable httpd.service
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[root@smshost ~]# sudo systemctl enable dhcpd.service 
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.

[root@smshost ~]# sudo systemctl enable tftp.socket 
Created symlink /etc/systemd/system/sockets.target.wants/tftp.socket → /usr/lib/systemd/system/tftp.socket.

[root@smshost ~]# sudo systemctl restart httpd 
[root@smshost ~]# sudo systemctl restart tftp.socket  
```

## 4.2 Define Compute Image

Now that the provisioning services are enabled, the next step is to define and customise a system image that can be used to provision one or more compute nodes. 

This process starts with defining a base operating system image using *Warewulf*. 

!!! note "Tip"

    It is important to understand that in this virtual lab example (and likely your physical HPC system deployment), your compute nodes will be provisioned using **stateless provisioning**. This means that rather than loading your operating system onto a persistent storage medium or disk, the operating system image is *loaded into memory* on boot.
    
    Provisioning your system this way *ensures parity* across your compute nodes (as the same image is deployed to each node on node boot) and means that changes made to the particular node's operating system do not survive after a reboot of that node (it is <u>non-persistent</u>). The OS image needs to be defined on the **smshost** VM so that *Warewulf* can repeatedly use this image to deploy new compute hosts. 

1.  The first step is to define a directory structure on the **smshost** that will represent the root filesystem for the compute node *(`chroot`)*. The default location in this example is `/opt/ohpc/admin/images/rocky8.5`.<div>

    ```bash
    [root@smshost ~]# export CHROOT=/opt/ohpc/admin/images/rocky8.5 
    ```

    To ensure that we always remember to source this path in future sessions, we will add it to `input.local.lab`:

    ```bash
    [root@smshost ~]# echo CHROOT=/opt/ohpc/admin/images/rocky8.5 >> input.local.lab
    ```

    Now, every time the `input.local.lab` file is sourced, the value for `$CHROOT` will be loaded into the environment variable.

2. Build the initial `chroot` image (provides a minimal Rocky 8.5 configuration)<div>

    ```bash
    [root@smshost ~]# wwmkchroot -v rocky-8 $CHROOT  # 81MB 
    ```

    ```bash
    # OUTPUT:
    Complete!
    == Running: postchroot
    == Running: configure_fstab
    == Running: configure_network
    == Running: configure_ntp
    == Running: configure_pam
    == Running: configure_authentication
    == Running: configure_sshkeys
    == Running: configure_rootacct
    == Running: configure_runlevel
    == Running: configure_services
    == Running: configure_timezone
    == Running: finalize
    == Running: cleanup
    ```
3. Enable the *OpenHPC* and EPEL repositories inside `chroot`<div>

    ```bash
    [root@smshost ~]# sudo dnf -y --installroot $CHROOT install epel-release
    
    # OUTPUT:
    # Installed:
    #     epel-release-8-18.el8.noarch
    
    [root@smshost ~]# sudo cp -p /etc/yum.repos.d/OpenHPC*.repo $CHROOT/etc/yum.repos.d
    ```

## 4.3 Add Compute Components

Now that we have a minimal *Rocky 8* image, we will add components needed for the compute nodes to function as part of the HPC cluster. These include:

* resource management client services, 
* NTP support, and 
* additional packages needed to support the OpenHPC environment. 

This process augments the `chroot`-based install performed by `wwmkchroot` by modifying the base provisioning image. 

1. Install the compute node base meta package<div>

    ```bash
    [root@smshost ~]# sudo dnf -y --installroot=$CHROOT install ohpc-base-compute 
    ```

2. Copy the **smshost** DNS configuration to the `chroot` environment <div>

    ```bash
    [root@smshost ~]# sudo cp -p /etc/resolv.conf $CHROOT/etc/resolv.conf
    ```

    !!! note "Tip"

        When doing this we are making the assumption that your **smshost** has a *working* DNS configuration. Please ensure that this is the case before completing this step. There are some tips on common Lab-related hiccups (including DNS) in the FAQ.

3. Copy the local user credential files into `chroot` to ensure consistent **uid/gids** for *Slurm* and *MUNGE* at install. <div>
    ``` bash
    [root@smshost ~]# sudo cp /etc/passwd /etc/group $CHROOT/etc

    # OUTPUT
    # cp: overwrite '/opt/ohpc/admin/images/rocky8.5/etc/passwd'? y
    # cp: overwrite '/opt/ohpc/admin/images/rocky8.5/etc/group'? y 
    ```

    !!! quote "Note"

        Future updates to your user credential files will be synchronised between hosts by your provisioning system.

4. Add the *Slurm* client and enable *MUNGE*<div>
    ```bash
    [root@smshost ~]# sudo dnf -y --installroot=$CHROOT install ohpc-slurm-client 
    [root@smshost ~]# sudo chroot $CHROOT systemctl enable munge

    # OUTPUT
    # Created symlink /etc/systemd/system/multi-user.target.wants/munge.service → /usr/lib/systemd/system/munge.service.
    ```

5. Register the *Slurm* server IP address for the compute nodes (using the configless option)<div>
    ```bash
    [root@smshost ~]# sudo echo SLURMD_OPTIONS="--conf-server ${sms_ip}" > $CHROOT/etc/sysconfig/slurmd
    ```

    !!! quote "Note"

        *Configless Slurm* is a *Slurm* feature that allows the `slurmd` process running on the compute nodes to pull the configuration information from `slurmctld` (on the **smshost**), rather than from a pre-distributed local file. For more information on how this feature works, see the [documentation](https://slurm.schedmd.com/configless_slurm.html).

6. Add Network Time Protocol (NTP) support and identify the **smshost** as a local NTP server<div>
    ```bash
    [root@smshost ~]# sudo dnf -y --installroot=$CHROOT install chrony
    [root@smshost ~]# sudo echo "server ${sms_ip} iburst" >> $CHROOT/etc/chrony.conf
    ```
7. Add kernel drivers (matching the kernel version on the **smshost**)<div>
    ```bash
    [root@smshost ~]# sudo dnf -y --installroot=$CHROOT install kernel-`uname -r`  # 276MB
    ```

    !!! NOTE
        If the kernel drivers step results in an error, typically:
        ```bash
        Last metadata expiration check: 0:10:30 ago on Thu 24 Aug 2023 08:43:11 PM UTC.
        No match for argument: kernel-4.18.0-425.3.1.el8.x86_64
        Error: Unable to find a match: kernel-4.18.0-425.3.1.el8.x86_64
        ```
        then the kernel drivers are not available at the repo defined on the **smshost**. 

        Try https://oraclelinux.pkgs.org/8/ol8-baseos-latest-x86_64/kernel-4.18.0-425.3.1.el8.x86_64.rpm.html

        ```bash
        sudo dnf -y --installroot=$CHROOT install kernel  # 276MB
        ``````

8. Include *modules* to the user environment<div>
    ```bash
    [root@smshost ~]# sudo dnf -y --installroot=$CHROOT install lmod-ohpc

    # OUTPUT
    # Installed:
    #   fish-3.3.1-2.el8.x86_64    lmod-ohpc-8.7.6-12.3.ohpc.2.6.x86_64            
    #   lua-5.3.4-12.el8.x86_64                        lua-filesystem-1.6.3-7.el8.x86_64
    #   lua-posix-33.3.1-9.el8.x86_64                  pcre2-utf32-10.32-3.el8_6.x86_64
    #   rc-1.7.4-11.el8.x86_64                         tcl-1:8.6.8-2.el8.x86_64
    #   tcsh-6.20.00-15.el8.x86_64
    #
    # Complete!
    ```
    

## 4.4 Customise Compute Configuration

Before we assemble the compute image, it is advantageous to perform any additional customisation within the `chroot` environment (that you created for configuring your compute instance). The following steps detail the process of: 
- adding a local ssh key created by *Warewulf* (to support remote access),
- enabling NFS mounting of a `$HOME` filesystem, and 
- adding the public *OpenHPC* install path (`/opt/ohpc/pub`) of the **smshost**.

!!! note "Tip"

    To ensure that you make the correct changes to your NFS client mounts, you should know what the `fstab` file looks like before configuring it. (This applies across the board, with all files you are editing, always!)

    Run the command `cat` on the file  `$CHROOT/etc/fstab` both before and after making changes to it in step 2 below to see what effect the changes have. 

1. Initialise the Warewulf database and ssh keys

    ```bash
    [root@smshost ~]# sudo wwinit database
    # OUTPUT:
    #
    # database:     Checking to see if RPM or capability 'mysql-server' is install NO
    # database:     Checking to see if RPM or capability 'mariadb-server' is insta OKd
    # database:     Activating Systemd unit: mariadb
    # database:      + /bin/systemctl -q restart mariadb.
    # service                   OK
    # database:      + mysqladmin --defaults-extra-file=/tmp/0.HmYBjDpgbXhN/my.cnf OK
    # database:     Database version: UNDEF (need to create database)
    # database:     Creating database schema
    # database:      + mysql --defaults-extra-file=/tmp/0.HmYBjDpgbXhN/my.cnf ware OK
    # localhost
    # database:     Configured user does not exist in database. Creating user.
    # database:      + mysql --defaults-extra-file=/tmp/0.HmYBjDpgbXhN/my.cnf ware OK
    # database:     DB root user does not exist in database. Creating root user.
    # database:      + mysql --defaults-extra-file=/tmp/0.HmYBjDpgbXhN/my.cnf ware OK
    # database:     Updating database permissions for base user
    # database:      + mysql --defaults-extra-file=/tmp/0.HmYBjDpgbXhN/my.cnf ware OK
    # database:     Updating database permissions for root user
    # database:      + mysql --defaults-extra-file=/tmp/0.HmYBjDpgbXhN/my.cnf ware OK
    # database:     Checking binstore kind                          SUCCESS
    # Done.

    [root@smshost ~]# sudo wwinit ssh_keys

    # OUTPUT:
    #
    # ssh_keys:     Checking ssh keys for root                                     OK
    # ssh_keys:     Checking root's ssh config                                     OK
    # ssh_keys:     Checking for default RSA host key for nodes                    NO
    # ssh_keys:     Creating default node ssh_host_rsa_key:
    # ssh_keys:      + ssh-keygen -q -t rsa -f /etc/warewulf/vnfs/ssh/ssh_host_rsa OK
    # ssh_keys:     Checking for default DSA host key for nodes                    NO
    # ssh_keys:     Creating default node ssh_host_dsa_key:
    # ssh_keys:      + ssh-keygen -q -t dsa -f /etc/warewulf/vnfs/ssh/ssh_host_dsa OK
    # ssh_keys:     Checking for default ECDSA host key for nodes                  NO
    # ssh_keys:     Creating default node ssh_host_ecdsa_key:
                                                                         OK
    # ssh_keys:     Checking for default Ed25519 host key for nodes                NO
    # ssh_keys:     Creating default node ssh_host_ed25519_key:
                                                                         OK
    # Done.
    ```

2. Add NFS client mounts of `/home` and `/opt/ohpc/pub` to base compute image

    ```bash
    [root@smshost ~]# sudo echo "${sms_ip}:/home /home nfs nfsvers=3,nodev,nosuid 0 0" >> $CHROOT/etc/fstab
    [root@smshost ~]# sudo echo "${sms_ip}:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=3,nodev 0 0" >> $CHROOT/etc/fstab 
    ```

3. Export `/home` and *OpenHPC* public packages from **smshost**

    ```bash
    [root@smshost ~]# sudo echo "/home *(rw,no_subtree_check,fsid=10,no_root_squash)" >> /etc/exports
    [root@smshost ~]# sudo echo "/opt/ohpc/pub *(ro,no_subtree_check,fsid=11)" >> /etc/exports  
    ```

    !!! danger "Important"
        The file sharing settings for `/home` allows any other machine on the same network as the 'public' interface of the **smshost** to mount `/home` - this should *not* be allowed in a production environment, but is sufficient for the Virtual Lab environment since there is no user-sensitive information that can be exploited on this virtual infrastructure.


4. Finalise NFS configuration and restart the *nfs-server* service to enforce the configuration changes made in the previous step.

    ```bash
    [root@smshost ~]# sudo exportfs -a
    [root@smshost ~]# sudo systemctl restart nfs-server  
    [root@smshost ~]# sudo systemctl enable nfs-server
    # OUTPUT:
    #
    # Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
    ```



## 4.5 Add Additional Components

At this stage of the system setup, the official *OpenHPC* recipe lists a number of *optional* customisations that you may want to make to your compute image. These customisations include:

- Add InfiniBand or Omni-Path drivers
- Increase memlock limits
- Restrict ssh access to compute resources
- Add BeeGFS client
- Add Lustre client
- Enable syslog forwarding
- Add Nagios Core monitoring
- Add ClusterShell
- Add mrsh
- Add genders
- Add ConMan
- Add GEOPM

!!! danger "Important"

    For conciseness, this Virtual Lab will not cover any of these optional extras. If you wish to add any of these additional components to your systems, please refer to the [official OpenHPC 2.x Install Recipe](https://openhpc.community/downloads/) for information and guidance on this process. 


## 4.6 Import Files

The *Warewulf* provisioning system includes functionality to export files from the provisioning server (the **smshost** in this case) to the managed hosts. This is a convenient way to distribute user credentials to the compute hosts in your cluster. Similarly, this functionality will be used to import the cryptographic key that the *MUNGE* authentication library requires to be available on each host in the resource management pool. 

1. Instruct *Warewulf* to import the local file-based credentials to stage them for distribution to each of the managed hosts:

    ```bash
    [root@smshost ~]# sudo wwsh file import /etc/passwd
    [root@smshost ~]# sudo wwsh file import /etc/group
    [root@smshost ~]# sudo wwsh file import /etc/shadow
    ```

2. Instruct *Warewulf* to import the *MUNGE* key to stage it for distribution to each of the managed hosts:

    ```bash
    [root@smshost ~]# sudo wwsh file import /etc/munge/munge.key
    ```
!!! quote "Note"

    MUNGE is a service for user and group authentication useful for  a cluster environment. To quote the `man` page:

    *MUNGE (MUNGE Uid 'N' Gid Emporium) is an authentication service for creating and validating credentials. It is designed to be highly scalable for use in an HPC cluster environment. It allows a process to authenticate the UID and GID of another local or remote process within a group of hosts having common users and groups.*

!!! example "Recap"
    In this chapter you successfully defined and configured the compute node image for provisioning by *Warewulf*! 

!!! success "Congratulations!"

    **You have reached the end of Chapter 4.**
    
    In the next chapter we will finalise the *Warewulf* configuration and boot up your compute nodes. 
