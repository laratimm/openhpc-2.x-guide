It is now time to begin the hands-on work! In this chapter you will set up your virtual lab environment in preparation for starting to provision your VMs in the next chapter.

## 2.1 Install VirtualBox
---

*VirtualBox* is used in this lab to host the virtual cluster.

The first step in deploying this cluster is deploying the management server (**smshost**) as a VM. Although this VM *can* be used as the final management server solution for a physical HPC system, we **do not recommend this** as a long term solution.

!!! quote "Note"

    This virtual management server is **not** required for the final, physical HPC system.

    The virtual **smshost** is a virtualised replica of the final physical management server that will be deployed during a physical HPC system deployment. That said, all steps followed in preparing the virtual smshost will be identical to those followed on the physical management server.

1. [Download VirtualBox](https://www.virtualbox.org/wiki/Downloads){:target="_blank"}
    - *VirtualBox* is a cross-platform type-2 hypervisor, available as Open Source Software under GPL version 2.
    - Supported host operating systems include Windows, Macintosh, Linux and Solaris.

    !!! danger "Important"

        Individuals who are completing this virtual lab on a Windows machine may have some trouble with the latest *VirtualBox* release.

        While every effort has been taken to ensure software compatibility with users' local environments, if any problems are experienced with the latest version of VirtualBox, it is recommended in that case to make use of the older [VirtualBox Version 6.0.24](https://www.virtualbox.org/wiki/Download_Old_Builds_6_0){:target="_blank"}, which has proven stable in our test environment.

    !!! note "Tip"

        Always be mindful to maintain the *precise version* of the software that is outlined in this guide. If no version is explicitly mentioned, then it is safe to assume that the most current version will suffice. That said, where possible, it is advised to use the same versions as were most current at the time of release of OpenHPC 2.x. 

2. Install VirtualBox

Once *VirtualBox* is installed, the framework for running VMs is in place. Although one could now manually create a VM inside the *VirtualBox* manager GUI (or via the command line), there is no gaurantee of parity between your VM and the VM expected by the guide. To ensure configuration parity between the VM on your host machine and the one referenced in this guide, *Vagrant* will be used.  

## 2.2 Install Vagrant
---

*Vagrant* provides an automated mechanism to build and maintain VMs across a range of virtualisation hypervisors. We will make use of *Vagrant* to configure the **smshost** VM to precise specifications for the guide (and later to spawn the compute VMs). This ensures that every user will have the same setup, every time. 

!!! quote "Note"

    As **VMs are not required** for the physical HPC deployment, **neither is *Vagrant***. The use of a virtual environment managed by *Vagrant* is simply to ensure a reproducible training experience.

    *Vagrant* is a provisioning tool that ensures that all instances of a vagrant-managed VM are identical upon each instantiation. This is incredibly useful to ensure parity across systems during training workshops but is not required for a physical HPC deployment.

1. [Download Vagrant](https://www.vagrantup.com/downloads){:target="_blank"} 
2. Install Vagrant 

    !!! note "Tip"

        Be sure to select the package that matches your **host machine's operating system**, not the Rocky Linux VM. Your system may need to be rebooted following install. 

## 2.3 Prepare VMs
---

For this virtual lab you will be using *Vagrant* to manage and configure your *VirtualBox* VMs. The file that *Vagrant* uses to configure this setup is called a **Vagrantfile**. We provide you with a preconfigured `Vagrantfile`, as well as other critical files required for this lab, via *Git*. 

The lab preparation files can be obtained in several ways, but we will show you how to use *Git* to clone the virtual lab repository which is the recommended method for this virtual lab.

### 2.3.1 Clone lab repository using Git (recommended)

This option will keep any updates or changes syncronised with your system. 

!!! note "Tip"

    Updating files in the middle of an OpenHPC deployment that uses *Vagrant* can cause stabilitiy issues and is not recommended. Nonetheless, when carefully managed, a cloned Git repository is the most suitable approach during a class setting to **facilitate live bug fixes** or updates to all participants.

**1. Install Git**

Depending on your operating system environment, you may have to choose on of the following options:

- [Git BASH for Windows](https://gitforwindows.org/){:target="_blank"}
- Git for Linux
- Git for MacOS


**2. Navigate to your preferred Git root for this lab**. 

For example: `<your_home_directory>/openhpc-2.x-virtual-lab/`

!!! quote "Note"

    The *Git root* will be the base file directory for the virtual cluster files, for example `/home/me/openhpc-2.x-virtual-lab/` or `c:/users/me/Desktop/openhpc-2.x-virtual-lab/`

!!! quote "Note"

    For the purposes of this guide the *Git root* will be `~/openhpc-2.x-virtual-lab/`

    The sample syntax `[git root]#` can be interpreted as a prompt at `~/openhpc-2.x-virtual-lab/`

**3. Clone the Git repository** 

The public repository for the virtual lab can be cloned using HTTPS:

```bash
[<your git root>]# git clone https://gitlab.com/hpc-ecosystems/training/openhpc-2.x-virtual-lab.git 
[<your git root>]# cd openhpc-2.x-virtual-lab
```

**4. View lab files**

The files you have cloned from the remote *Git* repository are now available on your local machine at the location `<git_root>/openhpc-2.x-virtual-lab/`.

The most important files at present are:

| File                                 | Description                                                                   |
|------------------------------------------|------------------------------------------------------------------------|
|`input.local.lab` | The configuration parameter script for the virtual lab. Adadped from the original *OpenHPC* file `input.local`|
|`Vagrantfile`| The *Vagrant* configuration file for setting up the lab virtual machines|
|`package.box`| A preconfigured *Vagrant* box for setting up the compute VMs|
|`slurm.conf.lab`| The customised *Slurm* configuration file for the virtual lab|

!!! success "Congratulations"

    Your virtual lab envionment is ready to deploy the virtual cluster!


!!! example "Recap"

    In this chapter the virtual lab environment was set up:

    *VirtualBox* - hypervisor used to host the virtual machines in the virtual cluster

    *Vagrant* - used to manage the configuration of virtual machines within *VirtualBox*

    `Vagrantfile` - the actual file used to define the parameters for the VMs used in the virtual lab

