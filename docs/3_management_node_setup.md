
This section will guide you through the setup of your **System Management Server (SMS) host** - the node that is responsible for *managing the virtual cluster*. This node is often sometimes called the management node, master node, boss node, or host node. We will call it the **smshost**. 

!!! quote "Note"

    Anticipated time to complete this chapter: TBC from user feedback. 

## 3.1 Deploy smshost
----

The `vagrant up` command will initialise the vagrant environment, download and initialise the base **smshost** VM, and configure this VM - according to the parameters set in the `Vagrantfile`. This process may take some time - depending on the speed of your internet connection. 

!!! danger "Important"

    Be careful when you run `vagrant up`.

    Make sure to run `vagrant up` from your *Git root* or *root lab directory*. *Vagrant* will initilalise the VM with your current working directory as a shared directory within the VM at `/vagrant/`. Doing this ensures that your VM has access to the configuration files that you downloaded from *Git* - `input.lab.local`, etc. 

!!! note "Tip"

    Running an `ls` command (or equivalent) should at least list the `Vagrantfile` in the current working directory. The `vagrant up` instruction will reference this configuration file. If it is not located in the current working directory, Vagrant will climb up the directory tree (towards the root) looking for the first `Vagrantfile` it can find. 
    
    This could lead to the wrong `Vagrantfile` being used, so please ensure the correct working directory before running `vagrant up`.

    See [this link](https://developer.hashicorp.com/vagrant/docs/vagrantfile){:target="_blank"} for more information.

1. Navigate to your lab root directory. ie. `~/openhpc-2.x-virtual-lab/` 
    <BR>

2. Run the command:
    ```bash
    [~/openhpc-2.x-virtual-lab/]$ vagrant up smshost
    ```
    The above command will have *Vagrant* read the parameters in the `Vagrantfile` for the **smshost**, create a *VirtualBox* VM definition (such as vCPUs, RAM, NICs etc.), download and install the *Rocky Linux* image into the VM, boot the VM and install any addtional stipulated packages.

    !!! quote "Note"

        The **Rocky Linux 8** image is approximately **680MB** in size. 

        Running the `vagrant up` command may fail with an error relating to *"The IP address configured for host-only network is not within the allowed ranges"*. This is a known issue with the lastest versions of *VirtualBox*. To resolve this problem please follow the [VirtualBox documentation](https://www.virtualbox.org/manual/ch06.html#network_hostonly) on the matter. 

    !!! danger "Important"

        This virtual lab uses a single `Vagrantfile` to manage the **smshost** VM as well as the compute VMs - known as a **multi-machine Vagrantfile**. To ensure you are accessing the correct VM definition and VM you are required to add the name of the host after any *Vagrant* commands. ie. `vagrant up smshost` or `vagrant ssh smshost`.

3. Once the VM is booted and the additional packages have been installed, you should be able to access your **smshost** VM via `ssh`. 

    !!! note "Tip"

        You can `ssh` to the VM at any time using one of the following methods:

        1. Using vagrant:
            ``` bash
            [~/openhpc-2.x-virtual-lab/]$ vagrant ssh smshost
            ```

        2. Using any SSH client to `127.0.0.1:229` with the default Vagrant credentials (username::password) `vagrant::vagrant`

        While `vagrant ssh` is the easiest method, you may or may not have clipboard copy/paste available on the VM.  

    !!! quote "Note"

        Your host machine has a shared directory with the VM as defined in the `Vagrantfile`. By default this directory is the location of the `Vagrantfile` on your workstation and is at `/vagrant/` on the VM. 

!!! success "Congratulations"

    You have deployed the **smshost** virtual machine!

## 3.2 Add and Configure Parameters
---

Prior to continuing with the installation of the *OpenHPC* components on the **smshost**, several commands must be issued to set up the shell environment. To ensure that all defined variables are set for the current shell environment, the configuration file must be `sourced`. 

!!! note "Tip"

    The official *OpenHPC* recipe mentions an `input.local` environment file. **This file is not present in this OpenHPC 2.x guide**. For the purposes of this guide, we are using `input.local.lab` in its place, which is a simplified pre-customised environment file for purposes of the labs contained in this guide.*

    In either case the configuration file (or local input file) must be sourced in the existing shell (ie. loaded into the *current* shell environment).

    If you make any updates to the configuration file, the `source` command must be run again. 

    Recall, `/vagrant/` on the VM is shared with the local host system at the directory location of your `Vagrantfile`. You will have pulled these configuration files together when cloning the lab *Git* repo. 

1. SSH to the **smshost** VM and elevate to `root`

    ``` bash
    [~/openhpc-2.x-virtual-lab/]$ vagrant ssh smshost

    [vagrant@smshost ~]$ sudo su

    [root@smshost ~]#
    ```

    !!! note "Tip"

        **Running commands as `root` is not best practice!**

        Since this is a test lab, it is considerably easier to issue commands as `root` and not have to worry about occasional `sudo` workarounds. In general, however, it is not recommended, and to ensure the best-practice habit of not running as `root`, we will still issue all commands with `sudo`, even when logged in as `root`.

2. Examine the current environment variable status

    ``` bash
    [root@smshost ~]# echo ${sms_name} 

    [root@smshost ~]# echo ${sms_ip} 

    ```

    Both commands will show no response, or rather, a *blank* response.

3. After sourcing the local input file, the *OpenHPC* environment variables return the definitions as sourced from the local input file `input.local.lab`.
<BR>

    ``` bash
    [root@smshost ~]# cd /vagrant
    [root@smshost vagrant]# source input.local.lab

    [root@smshost vagrant]# echo ${sms_name}
    # OUTPUT: smshost

    [root@smshost vagrant]# echo ${sms_ip}
    # OUTPUT: 10.10.10.10
    ```
    
4. Once the input file has been sourced, all the environment variables will be set for the *current* shell session. 

    !!! quote "Note"

        Every new shell instance must have the input file re-sourced. 


5. Add the DNS entry to the `/etc/hosts` file

    ``` bash
    [root@smshost ~]# sudo echo ${sms_ip} ${sms_name} >> /etc/hosts
    [root@smshost ~]# cat /etc/hosts

    # OUTPUT
    # ...
    # 10.10.10.10 smshost
    ```

6. *OpenHPC* recommends disabling *SELinux*

    ``` bash
    [root@smshost ~]# sudo sed -i "s/^SELINUX=.*/SELINUX=disabled/" /etc/selinux/config
    ```

7. Reboot the VM

    ``` bash
    [root@smshost ~]# sudo reboot
    ```

    !!! danger "Important"

        When rebooting the VM from within the guest OS, the `/vagrant` directory may not remap on reboot. If the mapping is lost, a `vagrant reload` will reload the configuration file.

        Alternatively, `vagrant halt` (graceful shutdown) followed by `vagrant up` will reboot the VM. This process also reloads the `Vagrantfile`, reestablishing the shared directory mapping.

        `vagrant reload` is functionally equivalent to `vagrant halt` followed by `vagrant up`

    !!! note "Tip"

        **Always run as `root` and re-source the environment variables on every new shell.**

        As stated before, this is a test environment, so it is considerably easier to bypass occasional `sudo` workarounds by running directly as `root`. This is not best practice in a production environment!

        *OpenHPC* makes extensive use of environment variable substitution. If you do not source the local input file `input.local.lab` then these variables will hold blank values and lead to unpredictable behaviour. 

8. On reboot, return to `root` profile and `source` the correct environment:

    ``` bash
    [vagrant@smshost ~]$ sudo su
    [root@smshost ~]# source /vagrant/input.local.lab
    ```

9. Disable the Firewall

    ``` bash
    [root@smshost ~]# sudo systemctl disable firewalld
    [root@smshost ~]# sudo systemctl stop firewalld
    ```

    !!! note "Tip"

        **It is *not* best practice to run a public-facing server without a firewall!**

        Again, since this is a test lab, it simplifies the learning objectives when we know that commands will not be blocked by an unexpected firewall issue. The *OpenHPC* install recipe follows this philosophy.

!!! success "Congratulations"

    You have now installed and prepared the **smshost** virtual machine for the *OpenHPC* components.

    The next step is to add the necessary packages in order to provision and manage the virtual cluster. 


## 3.3 Add OpenHPC Components
---

Now that the base operating system is installed and booted, the next step is to add the desired *OpenHPC* packages to the **smshost**. These packages will provide *provisioning* and *resource managment* services to the rest of the virtual cluster. 

### 3.3.1 OpenHPC Repository

You will need to enable the *OpenHPC* repository for local use - this requires external internet access from your master **smshost** to the *OpenHPC* repository which is hosted on the internet.

``` bash
[root@smshost ~]$ sudo dnf install -y http://repos.openhpc.community/OpenHPC/2/EL_8/x86_64/ohpc-release-2-1.el8.x86_64.rpm 
```
### 3.3.2 EPEL Release

In addition to the *OpenHPC* repository, the **smshost** needs access to other base OS distro repositories so that it can resolve the necessary dependencies. These include: 

* BaseOS
* Appstream
* Extras
* PowerTools, and 
* EPEL repositories

From the prior output, you may have noticed that `epel-release` is enabled automatically when installing `ohpc-release`. Unlike the other repositories which are enabled by default, `PowerTools` must be enabled from EPEL manually as follows:

```bash
[root@smshost ~]# sudo dnf -y install dnf-plugins-core 
[root@smshost ~]# sudo dnf -y config-manager --set-enabled powertools 
```

### 3.3.3 Provisioning and Resource Management

In this virtual lab, system provisioning and workload management will be performed using *Warewulf* and *Slurm*, respectively. 

To add support for *provisioning services*, one must add the common base package provided by *OpenHPC*, as well as the **Warewulf Provisioning System**. 

```bash
[root@smshost ~]# sudo dnf -y install ohpc-base      
[root@smshost ~]# sudo dnf -y install ohpc-warewulf
```

To add support for *workload management*, we will install the **Slurm Workload Manager**. Simply put, *Slurm* will perform the role of a *job scheduler* for our HPC cluster. Run the installation command:

```bash
[root@smshost ~]# sudo dnf -y install ohpc-slurm-server 
```

!!! note "Tip"

    The **smshost** acts as our *Slurm* server. The client-side components of the workload management system will be added to the corresponding compute image in the next chapter. 

!!! quote "Note"

    In order for the *Slurm* server to function correctly a number of things are required. 

    - It is essential that your *Slurm* configuration file, `slurm.conf`, is correctly configured. No need to worry! We will do this in the chapter on *Resource Management*. 
    - *Slurm* (and HPC systems in general) requires synchronised clocks throughout the system. We will utilise *NTP* for this purpose in the next section.

## 3.4 Configure Time Server
---

We will make use of the Network Time Protocol (NTP) to synchronise the clocks of all nodes on our virtual cluster. The following commands will enable NTP services on the **smshost** using the time server `${ntp_server}`, and allow this server to act as a local time server for the cluster. We will be using *chrony*, which is an alternative to *ntpd*.

```bash
[root@smshost ~]# sudo systemctl enable chronyd 
[root@smshost ~]# sudo echo "local stratum 10" >> /etc/chrony.conf 
[root@smshost ~]# sudo echo "server ${ntp_server}" >> /etc/chrony.conf 
```

The official *OpenHPC* recipe opts to allow all servers on the local network to synchronise with the **smshost**. In this lab, we will restrict the access to fixed IP addresses for our virtual cluster using the variable `cluster_ip_range`, as follows:

```bash
[root@smshost ~]# sudo echo "allow ${cluster_ip_range}" >> /etc/chrony.conf 
[root@smshost ~]# sudo systemctl restart chronyd
```

!!! success "Congratulations"

    **You have now successfully completed the basic configuration of your *smshost* !**

    Before moving on to the configuration of the compute images, we will quickly cover how one can make backups of progress throughout this virtual lab.

## 3.5 Snapshot **smshost** (Recommended)
---

While it is plausible to run the entire virtual lab without making any backups of your progress, it is recommended to at least make snapshots of major milestones (such as at the end of each chapter of this guide). Be aware that too many snapshots can bloat your resource usage and will increase the amount of disk space you will need to host the VMs. 

You can make snapshots using either the *Virtualbox* GUI or command line:

1. **Through the VirtualBox Manager GUI:**

    <center>[<img id="fig2" alt="Figure 2: How to snapshot a VM using the VirtualBox GUI" src="../_media/figure2.png" />]()</center>
    <center>Figure 2: How to snapshot a VM using the VirtualBox GUI</center>

2. **Through the command prompt:**
    <BR>
    Call the `snapshot save` instruction from the command line, followed by the `<vm_name>` (where applicable) and then the desired `<snapshot_name>`.
    <BR>

    Note: This is *not within* the **vagrant** session but *at the command prompt outside* the `vagrant ssh` session.
    <BR>
    ```bash
    [~/openhpc-2.x-virtual-lab/]$ vagrant snapshot save chapter3-smshost-complete
    ```

    ```bash
    ==> smshost: Snapshotting the machine as 'chapter3-smshost-complete'...
    ==> smshost: Snapshot saved! You can restore the snapshot at any time by
    ==> smshost: using `vagrant snapshot restore`. You can delete it using
    ==> smshost: `vagrant snapshot delete`.
    Machine 'compute00' has not been created yet, and therefore cannot save snapshots. Skipping...
    Machine 'compute01' has not been created yet, and therefore cannot save snapshots. Skipping...
    ```

    !!! quote "Note"

        Running the above command will take a snapshot of *all VMs* defined by your `Vagrantfile` at that present moment (see the sample output above, where `compute00` and `compute01` are skipped). It may be useful to only take a snapshot of a subset of your VMs later on (to avoid backing up VMs that have not changed configuration since the last snapshot). 

        To specify the VM that you want to snapshot:

        ```bash
        [~/openhpc-2.x-virtual-lab/]$ vagrant snapshot save <vm_name> <snapshot_name>
        ```

        For more information on how to manage and view your saved snapshots, see the *Vagrant* snapshot [documentation](https://developer.hashicorp.com/vagrant/docs/cli/snapshot){:target="_blank"}.

!!! success "Congratulations"

    **You have reached the end of Chapter 3 - Well done!**

    In this chapter you successfully deployed and configured your **smshost** VM. You are well on your way to your virtual cluster deployment. In the next chapter you will define and configure your compute node image. 