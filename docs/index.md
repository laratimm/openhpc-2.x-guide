# OpenHPC 2.x Guide 

**Welcome to the HPC Ecosystems OpenHPC 2.x Training Guide!** We are excited to have you along for the journey.

In the sections to follow you will find a comprehensive *HOW-TO* guide to set up **OpenHPC 2.x** on a virtual cluster. This guide is primarily developed for the **HPC Ecosystems Community** as training material to ensure parity across partner sites' HPC software environments.

That being said, you *DO NOT* need to be a member of the HPC Ecosystems Community to use or benefit from this guide! 

Anyone wishing to learn more about the deployment of the OpenHPC 2.x software stack is likely to find value in this content. 

Let’s go ahead and jump right in!
