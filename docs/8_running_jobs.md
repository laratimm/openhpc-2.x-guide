In this chapter you are going to run your first jobs on your virtual cluster. All the hard work you have put in has accumulated to this point where you now have a functional HPC system deployment! Let's get started!

!!! quote "Note"

    Anticipated time to complete this chapter: ???. 


## 8.1 Interactive Job

In Chapter 5 you prepared your resource manager and created a `test` user to run jobs. Before running jobs you must log in as this user, and compile your application. 

1. Switch to `test` user <div>

    ```bash
    [root@smshost ~]# sudo su - test
    Configuring SSH for cluster access
    [test@smshost ~]$
    ```

2. Compile MPI 'hello world' example:<div>

    !!! quote Users on the HPC system
        Everything performed as `test` simulates what a traditional user on an HPC user can do. In this virtual lab, this includes:

        - Compiling source code
        - Submitting a binary to the `Slurm` job scheduler
        - Querying the `Slurm` workload manager


    ```bash
    [test@smshost ~]$ mpicc -O3 /opt/ohpc/pub/examples/mpi/hello.c 
    ```

    !!! note "Note"

        The file `hello.c` is a simple 'hello world' application, provided by *OpenHPC*, that can be used as a test job for quick compilation  and execution. 
        
        *OpenHPC* also provides a companion job-launch utility named `prun` that is installed along with the pre-packaged MPI toolchains. 
        
        This convenience script provides a mechanism to abstract job launch across different resource managers and MPI stacks, such that a single launch command can be used for parallel job launch in a variety of *OpenHPC* environments.

3. Submit an interactive job request and verify allocation.
   
    !!! note Interactive Jobs
        An interactive job request is submitted to *Slurm* to request an allocation of resources to the user; if the request is granted, then the user has direct access to the compute resource (typically a `bash` shell).

    ``` bash
    [test@smshost ~]$ salloc -n 4 -N 2 
    # OUTPUT
    # salloc: Granted job allocation <jobID> 
    ```
    To verify / query the number of nodes (and which nodes), a user can run `squeue`, to list some information about the current interactive job:<div>

    ```bash
    [test@smshost ~]$ squeue
    # OUTPUT
    # JOBID   PARTITION       NAME   USER ST   TIME  NODES   NODELIST(REASON)
    # <jobID>    normal   interact   test  R   0:03      2     compute[00-01]
    ```

    !!! note Interpreting 'squeue'
        `squeue` will list some pertinent information to the user, including:

        - `JOBID` - useful for job output logs, controlling jobs, etc.
        - `ST` - the state of the current job (such as `R` for **RUNNING**)
        - `NODES` - the number of nodes reserved for this job
        - `NODELIST` - the names of the nodes reserved for this job
        <br>

4. Use `prun` to launch executable. <div>

    ``` bash
    [test@smshost ~]$  prun ./a.out
    # OUTPUT
    # [prun] Master compute host = smshost  
    # [prun] Resource manager = slurm  
    # [prun] Launch cmd = mpirun ./a.out (family=openmpi4)  

    # Hello, world (4 procs total)  
    # --> Process #   1 of   4 is alive. -> compute00  
    # --> Process #   0 of   4 is alive. -> compute00  
    # --> Process #   2 of   4 is alive. -> compute01  
    # --> Process #   3 of   4 is alive. -> compute01 
    ```

    !!! note ORTE daemon error

        At this point you may experience an error along the lines of *"An ORTE daemon has unexpectedly failed after launch and before communicating back to mpirun"*. This is a known issue, where the `/etc/hosts` file is incorrectly populated by Vagrant on VM initialisation. Please ensure that your hosts file is correctly populated and try again. 

        If you look at the contents of `/etc/hosts` and find that the defintion for `smshost` is present for both `127.0.0.0/24` and `10.10.10.10`, the clients will query the `hosts` file FIFO, so there is a risk that `smshost` will resolve to the `localhost` IP range first, instead of `10.10.10.10`. You can remove the `localhost` range from the `/etc/hosts` file on the compute nodes or ensure the ordering is correct.

    !!! danger "Important"

        The resources that you have allocated to run this interactive job are still reserved! You will need to free them to run any further jobs.

        This can be done using the `squeue` and `scancel` commands provided by *Slurm*. 
        
        The command `squeue` will help you identify the ID of the job that you wish to cancel and `scancel <jobID>` will allow you to cancel the job and free up the resources. 

        ```bash
        # EXAMPLE
        #
        # [test@smshost ~]$ scancel 1
        # salloc: Job allocation 1 has been revoked.
        # Hangup
        ```
        You may need to press **CTRL+D** to disconnect if it does not automatically disconnect.

    Once the interactive job is completed and successfully revoked, you can verify the state of the *Slurm* queue by running `sinfo`. If the available resources are returned to `idle` in the **STATE** column, then the nodes are ready to accept new jobs.

    If there are any issues with the job queue, you may need to manually reset the compute resources:

    ```bash
    sudo scontrol update nodename=compute[00-01] state=down reason="not behaving"
    sudo scontrol update nodename=compute[00-01] state=resume
    ```

!!! success "Congratulations"
    You have a functioning (although, not yet finished) cluster!

!!! example Recap: Chapter 8

    You have successfully run an interactive job on your cluster. This means that users of your cluster can now successfully run their own interactive jobs using the system scheduler, *Slurm*. 

## 8.2 Batch Job

A batch job is submitted to the queue and executed at an undefined time in the future - there is no user interaction with the compute resources, and when a job terminates (completes, crashes, or expires), an output file can be reviewed by the user to determine the outcome of the job submission.

Once again, before running your batch job, you need to ensure you are logged in as the `test` user and that you have compiled your application. 

An example job script is provided by *OpenHPC*. In this job script you will see that the same pre-compiled executable from the interactive job example is used. You will not have to repeat the compilation step in this section. We will copy and modify this file to match our lab configuration.

1. Copy example job script to the current directory.

    ``` bash
    [test@smshost ~]$  cp /opt/ohpc/pub/examples/slurm/job.mpi . 
    ```

2. View the example job script.

    ``` bash
    [test@smshost ~]$  cat job.mpi
    ```
    Which should look like:
    ``` bash 
    #!/bin/bash  
    #SBATCH -J test        # Job name  
    #SBATCH -o job.%j.out  # Name of stdout output file (%j expands to jobId)  
    #SBATCH -N 2           # Total number of nodes requested  
    #SBATCH -n 16          # Total number of mpi tasks requested  
    #SBATCH -t 01:30:00    # Run time (hh:mm:ss) - 1.5 hours  

    # Launch MPI-based executable  
    prun ./a.out 
    ```

    !!! danger "Important"

        Recall that your virtual cluster consists of two compute nodes, each with a single socket, and two cores per socket (See **Chapter 6**). 
        
        In order for MPI to parse our job script and run the application, the job script must match the parameters of the compute hosts. 
        
        If this is not properly configured, your job will not run, because the workload manager will hold the job in the queue until adequate resources become available (which will never happen).

    !!! note "Tip"

        The use of the `%j` option in the example batch job script is a convenient way to track the output of the application on an individual job basis. The `%j` token is replaced with the *Slurm* job allocation number, `<jobID>`, once the job is assigned.

3. Update the job script for our configuration, and verify the change:

    -  Change the number of mpi tasks from 16 to 4 (2 compute nodes * 2 cores each = 4 mpi tasks).<div>
  

    ``` bash
    [test@smshost ~]$  perl -pi -e "s/-n 16/-n 4 /" ./job.mpi 
    [test@smshost ~]$   cat job.mpi | grep tasks
    # OUTPUT:
    # #SBATCH -n 4                  # Total number of mpi tasks requested
    ```

4. Launch your batch job via *Slurm* by using the `sbatch` command.

    ``` bash
    [test@smshost ~]$  sbatch job.mpi 
    # Output: Submitted batch job <jobID> 
    ```

5. Once complete, view the job output, found in the file `job.<jobID>.out `.<div>


    If successful you can expect an output similar to the following:

    ``` bash 
    # [prun] Master compute host = compute00  
    # [prun] Resource manager = slurm  
    # [prun] Launch cmd = mpirun ./a.out (family=openmpi4)  
    #
    # Hello, world (4 procs total)  
    #     --> Process #   1 of   4 is alive. -> compute00  
    #     --> Process #   0 of   4 is alive. -> compute00  
    #     --> Process #   2 of   4 is alive. -> compute01  
    #     --> Process #   3 of   4 is alive. -> compute01 
    ```

!!! example Recap: Chapter 8
    You have successfully run a batch submission job on your cluster! This means that users of the cluster can now successfully submit their own jobs for processing and executing on the cluster using Slurm. 

!!! success "Congratulations"
    You have reached the end of the Virtual Lab!

    Have a cake!

    Fill out our survey!

    Let us know what you think!
    

    